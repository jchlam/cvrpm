#include <stdio.h>
#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <ctime>
#include <fstream>

using namespace cv;
using namespace std;

#define ANG_BIN             10
#define SAMPLE_LEN          30
#define inc(x)              x+1<SAMPLE_LEN?x+1:0
#define angDiff(x,y)        x-y > M_PI ? (x-y)-2*M_PI : ((x-y) < (-M_PI) ? (x-y) + 2*M_PI : x-y)

RNG rng(12345);

Mat translateImg(Mat &img, int offsetx, int offsety){
    Mat trans_mat = (Mat_<double>(2,3) << 1, 0, offsetx, 0, 1, offsety);
    warpAffine(img,img,trans_mat,img.size());
    return trans_mat;
}

int main(int argc, char** argv )
{
    ofstream outfile;
    outfile.open(argv[1], std::ios_base::app);
    VideoCapture stream1(1);   //0 is the id of video device.0 if you have only one camera.
    if (!stream1.isOpened()) { //check if video device has been initialised
        cout << "cannot open camera";
    }
    
    double t = (double)getTickCount() / (double)getTickFrequency();
    
    double prevAng = 0;
    double prevT = t;
    double sumAngDiff = 0;
    double angDiffHist[SAMPLE_LEN];
    double timeHist[SAMPLE_LEN];
    double lapTime[ANG_BIN];
    int head = 0;
    
    for(int i=0;i<ANG_BIN;i++){
        lapTime[i] = t;
    }

    for(int i=0;i<SAMPLE_LEN;i++){
        angDiffHist[i] = 0;
        timeHist[i] = 0;
    }

    Mat cam1;
    Mat src_gray;
    Mat graph =  Mat::zeros( 400, 1360, CV_8UC3 );
    double fps = 0;
    //unconditional loop
    while (true) {
        //fps = fps * 0.95 + cv::getTickFrequency() /  ((double)cv::getTickCount() - t)* 0.05;
        //cout << "I am working at " << fps << " FPS" << endl;
        
        //vector<Vec3f> circles;
        stream1.read(cam1);
        t = (double)getTickCount() / (double)getTickFrequency();
        cvtColor( cam1, src_gray, CV_BGR2GRAY );
        adaptiveThreshold(src_gray, src_gray,255,CV_ADAPTIVE_THRESH_MEAN_C, CV_THRESH_BINARY,45,5);
        medianBlur(src_gray,src_gray,3);
        //Mat element = getStructuringElement( MORPH_ELLIPSE,
        //                                        Size(5,5),
        //                                        Point(2,2));
        // Apply the dilation operation
        //dilate( src_gray, src_gray, element );
        //erode( src_gray, src_gray, element );
       
        vector<vector<Point> > contours;
        vector<Vec4i> hierarchy;
        
        imshow("cam2", src_gray);
        findContours( src_gray, contours, hierarchy, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE, Point(0, 0) );
        
        vector<vector<int> > hull(contours.size());
        vector<vector<Vec4i> > cvDft(contours.size());
        
        RotatedRect rotRecs[contours.size()];
        for( int i = 0; i< contours.size(); i++ ){
            //check that there are children
            if(contours[i].size() > 60 && hierarchy[i][2] > 0){
                double area = contourArea(contours[i]);
		if(area > 1000 && area < 3000){
                    //if(!isContourConvex(contours[i]))continue;
                    Scalar color = Scalar( 255, 0, 255 );
                    convexHull( contours[i], hull[i]);
                    convexityDefects(contours[i], hull[i], cvDft[i]);
                    float err = 0;
                    for(int k=0;k<cvDft[i].size();k++){
                        err += abs(cvDft[i][k][3]/256.0);
                    }
                    if(err / (cvDft[i].size()) > 2.7 )continue; 
                    if(rotRecs[i].center.x == 0 && rotRecs[i].center.y ==0){
                        rotRecs[i] = fitEllipse(contours[i]);
                    }
		    Point2f off = Point2f(0,0);
                    bool centered = false;
                    for(int j=hierarchy[i][2];j > 0;j=hierarchy[j][0]){
                        if(rotRecs[j].center.x == 0 && rotRecs[j].center.y ==0 && contours[j].size() > 15){
                            rotRecs[j] = fitEllipse(contours[j]);
                        }
                        double wo = norm(Point2f(rotRecs[i].size.width,rotRecs[i].size.height));
                        double wi = norm(Point2f(rotRecs[j].size.width,rotRecs[j].size.height));
                        if( norm(rotRecs[i].center - rotRecs[j].center) < 1 && wo/wi > 2.4 && wo/wi < 2.8){
                            off = rotRecs[i].center - rotRecs[j].center;
                            centered = true;
                            break;
                        }
                    }
                    if(!centered)continue;
                    
                    //RPM code here 
                    double angle = atan2(cam1.size().height/2 - rotRecs[i].center.y,cam1.size().width/2 - rotRecs[i].center.x);
                    //cout << "angle:" << angle << ",time:" << t << endl;
                    line( cam1,  Point(cam1.size().width/2,cam1.size().height/2), Point(cam1.size().width/2 - cos(angle) * 100,cam1.size().height/2 - sin(angle) * 100), Scalar(0,255,0), 1, 8 );
                    double timeDiff = (t - timeHist[head]) ;
                    timeHist[head] = t;
                    
                    double lapT = -1;
                    for(int k=0;k<ANG_BIN;k++){
                        line( cam1,  Point(cam1.size().width/2,cam1.size().height/2), Point(cam1.size().width/2 - cos(M_PI*2*k/ANG_BIN) * 100,cam1.size().height/2 - sin(M_PI*2*k/ANG_BIN) * 100), Scalar(255,0,0), 1, 8 );
                        double da = angDiff(prevAng,M_PI*2*k/ANG_BIN);
                        double db = angDiff(M_PI*2*k/ANG_BIN,angle);
                        if( abs(da) < (M_PI/2)&& !((da > 0) ^  (db> 0))){
                            double ratio = db / (da + db);
                            double tCross = t - (t-prevT) * ratio;
                            lapT = tCross - lapTime[k];
                            lapTime[k] = tCross;
                            double dps_hs = 360 / lapT ;
                            if(da > 0) dps_hs *= -1;
                            //cout << "DPS_HS:" << dps_hs << endl;
                        }
                    }

                    sumAngDiff -= angDiffHist[head];
                    angDiffHist[head] = angDiff(angle,prevAng);
                    sumAngDiff += angDiffHist[head];
                    prevAng = angle;
                    prevT = t;
                    head = inc(head);

                    
                    double dps = (sumAngDiff * (180/M_PI) / timeDiff) ;
                    cout.precision(15);
                    if(head == 0)
                        cout << "TIME:" << t << ",DPS:" << dps  << endl;
                    outfile.precision(15);
                    outfile << t << "," << dps << endl;
                    circle(graph, Point(graph.size().width,graph.size().height/2 - (dps * 10)), 1, Scalar(0,255,0));

                    //rotRecs[i].angle = atan2(off.y,off.x)/M_PI*180.0;
                    float angR = rotRecs[i].angle/180.0 * M_PI;
                    Point2f rect_points[4];
                    rotRecs[i].points( rect_points );

                    Point2f rect2_points[4];
                    rect2_points[0] = Point2f(0,0);
                    rect2_points[1] = Point2f(0,100);
                    rect2_points[2] = Point2f(100,100);
                    rect2_points[3] = Point2f(100,0);
                    //vector<Point2f> circ;
                    //vector<Point2f> oval;
                    //for(int j=0;j<contours[i].size();j++){
                    //    oval.push_back(Point2f(contours[i][j].x,contours[i][j].y));
                    //    circ.push_back(Point2f(50,50)+50*Point2f(cos((double)j/contours[i].size()*(2*M_PI)),sin((double)j/contours[i].size()*(2*M_PI))));
                    //}
                    
                    cv::Mat transmtx = cv::getPerspectiveTransform(rect_points, rect2_points);
                    cv::Mat quad = cv::Mat::zeros(100, 100, CV_8UC3);
                    //Mat transmtx =findHomography(oval,circ,0,1);
                    line( cam1, rotRecs[i].center, rotRecs[i].center + off*10, color, 1, 8 );
                    //line( cam1, rotRecs[i].center-rotRecs[i].size.width/2*Point2f(-cos(angR),-sin(angR)), rotRecs[i].center + rotRecs[i].size.width/2*Point2f(-cos(angR),-sin(angR)), color, 1, 8 );
                    cv::warpPerspective(cam1, quad, transmtx, quad.size());
                    
                    double mn = mean(quad).val[0];
                    threshold( quad, quad, mn, 255,0);
                    //cv::imshow("quad", quad);
                    //IplImage tmp=quad;
                    //cvLinearPolar(&tmp,&tmp, Point2f(50,50), 50,CV_INTER_LINEAR+CV_WARP_FILL_OUTLIERS );
                    //Mat out=&tmp;
                    //cv::imshow("quadrilateral", out);

                    for( int j = 0; j < 4; j++ )
                        line( cam1, rect_points[j], rect_points[(j+1)%4], color, 1, 8 );
                    drawContours( cam1, contours, i, color, 2, 8, hierarchy, 0, Point() );
                }
            }
        }
        
        line( cam1,  Point(cam1.size().width/2 + 50,cam1.size().height/2), Point(cam1.size().width/2 - 50,cam1.size().height/2), Scalar(0,0,255), 1, 8 );
        line( cam1,  Point(cam1.size().width/2,cam1.size().height/2 + 50), Point(cam1.size().width/2,cam1.size().height/2-50), Scalar(0,0,255), 1, 8 );
        circle(cam1, Point(cam1.size().width/2,cam1.size().height/2), 5, Scalar(0,255,0));
        imshow("cam", cam1);
        translateImg(graph,-1,0);
        line( graph,  Point(0,graph.size().height/2), Point(graph.size().width,graph.size().height/2), Scalar(100,100,100), 1, 8 );
        imshow("plot", graph);
        //imshow("cam2",src_gray);
        if (waitKey(1) == 'q')
            break;
    }
    return 0;
}

